let a = 5;
let b = 3;

function plus(){
    return a+b;
}
function minus(){
    return a-b;
}
function ride(){
    return a*b;
}
function divide(){
    return a/b;
}

module.exports ={
    plus:plus,
    minus:minus,
    ride:ride,
    divide:divide
}